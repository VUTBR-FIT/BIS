#!/usr/bin/env python3

import email
import os
import io
import gc
import json
import argparse
from collections import Counter

learningMode = False
statFileName = 'stat_data'
countWords = 4000

HAM = "ham"
SPAM = "spam"
MIN_WORDS = 1
MAX_WORDS = 3
SOURCES = [
    ('emails/kitchen-l', HAM),
    ('emails/lokay-m', HAM),
    ('emails/williams-w3', HAM),
    ('emails/GP', SPAM)
]
SOURCES2 = [
    #('emails/beck-s', HAM),
    #('emails/farmer-d', HAM),
    #('emails/kaminski-v', HAM),
    ('emails/ham', HAM),
    ('emails/ham_cs', HAM),
    ('emails/ham2', HAM),
    #('emails/BG', SPAM),
    #('emails/SH', SPAM),
    ('emails/spam', SPAM),
    ('emails/spam_cs', SPAM)
]


def saveData(data, index):
    """
    Ulozi vzorova data do souboru
    :param data:
    :return:
    """

    with io.open(statFileName + "_" + index + ".json", 'w+', encoding='utf8') as handle:
        handle.write(json.dumps(data))


def loadData(index):
    """
    Nacte vzorova data ze souboru
    :return list|dict:
    """
    with io.open(statFileName + "_" + index + ".json", 'r', encoding='utf8') as handle:
        text = json.load(handle)
    return text

def isValueValid(value):
    if value.isnumeric() is True:
        return False
    if len(str(value)) <= 2:
        return False
    return True


def parseFiles(dirPath):
    for filePath in getFiles(dirPath):
        yield filePath, parseFile(filePath)


def parseFile(filePath):
    try:
        content = getBody(getEmail(filePath))
    except UnicodeDecodeError:
        content = ""
    content = content.lower()
    result = []
    temp = []
    for word in content.split():
        temp.append(word.lower())
        if len(temp) >= MIN_WORDS:
            for i in range(MIN_WORDS, len(temp)):
                value = " ".join(temp[i:len(temp)])
                if isValueValid(value):
                    result.append(value)
        if len(temp) > MAX_WORDS:
            temp = temp[1:]
    return result

def mergeDictionary(dictionary, dictionary2):
    print(dictionary.keys())
    keysSpam = list(dictionary['spam'].keys())
    keysHam = list(dictionary['ham'].keys())

    print(len(dictionary[SPAM]))
    print(len(dictionary2[SPAM]))

    for key in dictionary2['ham'].keys():
        if key in keysHam:
            dictionary['ham'][key] += dictionary2['ham'].get(key)
        else:
            dictionary['ham'][key] = dictionary2['ham'].get(key)
    for key in dictionary2['spam'].keys():
        if key in keysSpam:
            dictionary['spam'][key] += dictionary2['spam'].get(key)
        else:
            dictionary['spam'][key] = dictionary2['spam'].get(key)

    temp1 = sum(dictionary['ham'].values())
    dictionary['ham'] = {key: (value / temp1) for key, value in dictionary['ham'].items()}
    temp1 = sum(dictionary['spam'].values())
    dictionary['spam'] = {key: (value / temp1) for key, value in dictionary['spam'].items()}
    return dictionary


def buildDictionary(sources):
    data = {'ham': {}, 'spam': {}}
    temp = {'ham': [], 'spam': []}
    mode = 1
    for path, classification in sources:
        for filePath, content in parseFiles(path):
            temp[classification] += content
    print('01')

    data['ham'] = validateDictionary(dict(Counter(temp['ham'])))
    data['spam'] = validateDictionary(dict(Counter(temp['spam'])))
    print('02')
    for value in list(data['spam'].keys()):
        if value in data['ham'].keys():
            tempHam = data['ham'].get(value)
            tempSpam = data['spam'].get(value)
            if (5*tempHam) > tempSpam:
                data['spam'].pop(value)
            else:
                data['ham'].pop(value)
    print('03')

    temp1 = sum(data['ham'].values())
    data['ham'] = {key: (value / temp1) for key, value in data['ham'].items()}
    temp1 = sum(data['spam'].values())
    data['spam'] = {key: (value / temp1) for key, value in data['spam'].items()}
    return data


def validateDictionary(dictionary):
    result = {}
    for value in dictionary.keys():
        if isValueValid(value):
            result[value] = dictionary[value]
    return result


def getFileContent(path):
    """
    Ziska obsah souboru
    :param string path Cesta k souboru:
    :return string Obsah souboru:
    """
    file = open(path, 'rb')
    content = file.read()
    file.close()
    return content


def getEmail(path):
    """
    Zpracuje email
    :param string path Cesta k emailu:
    :return mixed:
    :except UnicodeDecodeError
    """
    return email.message_from_string(getFileContent(path).decode("utf-8"))


def getBody(objEmail):
    """
    Ziska telo emailu
    :param mixed objEmail:
    :return string Telo emailu:
    """
    if objEmail.is_multipart():
        return ""
        ''' todo for payload in objEmail.get_payload():
            print(payload.get_payload())
            content += payload.get_payload()'''
    else:
        return objEmail.get_payload()


def getFiles(dirPath):
    '''
    Projde slozku a jeji podadresare a vrati vsechny soubory
    :param dirPath:
    :return:
    '''
    files = [os.path.join(dirPath, f) for f in os.listdir(dirPath) if os.path.isfile(os.path.join(dirPath, f))]
    subDirs = [os.path.join(dirPath, name) for name in os.listdir(dirPath) if
               os.path.isdir(os.path.join(dirPath, name))]
    for subDir in subDirs:
        files = files + getFiles(subDir)
    return files


def checkEmail(dictionary, fileStats):
    fileStats = dict(Counter(fileStats))
    parsedStats = {key: value for key, value in fileStats.items() if isValueValid(key) is True and (key in dictionary['ham'].keys() or key in dictionary['spam'].keys())}

    stats = {'ham': 0.0, 'spam': 0.0}
    for key, value in parsedStats.items():
        if key in dictionary['ham'].keys():
            stats['ham'] += value * dictionary['ham'].get(key)
        else:
            stats['spam'] += value * dictionary['spam'].get(key)
    if (3 * stats['ham']) > stats['spam']:
        return "HAM"
    else:
        return "SPAM"


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('filePaths', metavar='file', type=str, nargs='*', help='input file')
    parser.add_argument('--learning', help='enable learning mode', action='store_true')

    args = parser.parse_args()

    learningMode = args.learning

    if learningMode:
        dictionary1 = buildDictionary(SOURCES2)
        saveData(dictionary1, '01')
        dictionary1 = None
        gc.collect()

        #dictionary2 = buildDictionary(SOURCES2)
        #saveData(dictionary2, '02')
        #dictionary2 = None
        #gc.collect()

    else:
        dictionary1 = loadData('01')
        #dictionary2 = loadData('02')

        #dictionary = mergeDictionary(dictionary1, dictionary2)
        #dictionary1 = None
        #dictionary2 = None
        #gc.collect()

        for filePath in args.filePaths:
            print(filePath, '-', checkEmail(dictionary1, parseFile(filePath)))

